"""Finish all TODO items in this file to complete the isolation project, then
test your agent's strength against a set of known agents using tournament.py
and include the results in your report.
"""
import random
from isolation import *


class SearchTimeout(Exception):
    """Subclass base exception for code clarity. """
    pass


def keep_distance_short(game, player):
    """
    Gives feedback about the distance of the player to its opponent
    """
    moves = game.get_legal_moves(player)
    boarders = [0, game.width]
    utility = 0
    for move in moves:
        if move[0] in boarders or move[0] in boarders:
            utility = - 1

    return utility


def same_moves(game, player):
    """
    Checks all empty fields that could be caught in the next 2 rounds
    """
    player_moves = game.get_legal_moves(player)
    opp_moves = game.get_legal_moves(game.get_opponent(player))
    return len(set(player_moves).intersection(opp_moves))


def distance_mid(game, player):
    """
    Checks the distance to the middle
    """
    w, h = game.width / 2., game.height / 2.
    y, x = game.get_player_location(player)
    return float((h - y) ** 2 + (w - x) ** 2)


def possible_mid_moves(game, player):
    moves = game.get_legal_moves(player)
    value = 0
    for move in moves:
        if move[0] in [2, 3, 4] and move[1] in [2, 3, 4]:
            value = + 1
    return value


def custom_score(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.

    This should be the best heuristic function for your project submission.

    Note: this function should be called from within a Player instance as
    `self.score()` -- you should not need to call this function directly.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    # TODO: finish this function!
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    player_move = len(game.get_legal_moves(player))
    opponent_move = 0.00001 + len(game.get_legal_moves(game.get_opponent(player)))

    return (player_move ** 2 / opponent_move ** 2 + same_moves(game, player))


def custom_score_2(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.

    Note: this function should be called from within a Player instance as
    `self.score()` -- you should not need to call this function directly.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    # TODO: finish this function!
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    player_move = len(game.get_legal_moves(player))
    opponent_move = 0.00000001 + len(game.get_legal_moves(game.get_opponent(player)))
    return (player_move ** 2 / opponent_move ** 2 + possible_mid_moves(game, player))


def custom_score_3(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.

    Note: this function should be called from within a Player instance as
    `self.score()` -- you should not need to call this function directly.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """
    # TODO: finish this function!
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    player_move = len(game.get_legal_moves(player))
    opponent_move = 0.00000001 + len(game.get_legal_moves(game.get_opponent(player)))

    return (player_move ** 2 / opponent_move + possible_mid_moves(game, player) ** 2)


class IsolationPlayer:
    """Base class for minimax and alphabeta agents -- this class is never
    constructed or tested directly.

    ********************  DO NOT MODIFY THIS CLASS  ********************

    Parameters
    ----------
    search_depth : int (optional)
        A strictly positive integer (i.e., 1, 2, 3,...) for the number of
        layers in the game tree to explore for fixed-depth search. (i.e., a
        depth of one (1) would only explore the immediate sucessors of the
        current state.)

    score_fn : callable (optional)
        A function to use for heuristic evaluation of game states.

    timeout : float (optional)
        Time remaining (in milliseconds) when search is aborted. Should be a
        positive value large enough to allow the function to return before the
        timer expires.
    """

    def __init__(self, search_depth=3, score_fn=custom_score, timeout=10.):
        self.search_depth = search_depth
        self.score = score_fn
        self.time_left = None
        self.TIMER_THRESHOLD = timeout


class MinimaxPlayer(IsolationPlayer):
    """Game-playing agent that chooses a move using depth-limited minimax
    search. You must finish and test this player to make sure it properly uses
    minimax to return a good move before the search time limit expires.
    """

    def get_move(self, game, time_left):
        """Search for the best move from the available legal moves and return a
        result before the time limit expires.

        **************  YOU DO NOT NEED TO MODIFY THIS FUNCTION  *************

        For fixed-depth search, this function simply wraps the call to the
        minimax method, but this method provides a common interface for all
        Isolation agents, and you will replace it in the AlphaBetaPlayer with
        iterative deepening search.

        Parameters
        ----------
        game : `isolation.Board`
            An instance of `isolation.Board` encoding the current state of the
            game (e.g., player locations and blocked cells).

        time_left : callable
            A function that returns the number of milliseconds left in the
            current turn. Returning with any less than 0 ms remaining forfeits
            the game.

        Returns
        -------
        (int, int)
            Board coordinates corresponding to a legal move; may return
            (-1, -1) if there are no available legal moves.
        """
        self.time_left = time_left
        best_move = (-1, -1)

        try:
            return self.minimax(game, self.search_depth)

        except SearchTimeout:
            pass

        return best_move

    def minimax(self, game, depth):
        """Implement depth-limited minimax search algorithm as described in
        the lectures.

        This should be a modified version of MINIMAX-DECISION in the AIMA text.
        https://github.com/aimacode/aima-pseudocode/blob/master/md/Minimax-Decision.md

        **********************************************************************
            You MAY add additional methods to this class, or define helper
                 functions to implement the required functionality.
        **********************************************************************

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        Returns
        -------
        (int, int)
            The board coordinates of the best move found in the current search;
            (-1, -1) if there are no legal moves

        Notes
        -----
            (1) You MUST use the `self.score()` method for board evaluation
                to pass the project tests; you cannot call any other evaluation
                function directly.

            (2) If you use any helper functions (e.g., as shown in the AIMA
                pseudocode) then you must copy the timer check into the top of
                each helper function or else your agent will timeout during
                testing.
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise SearchTimeout()

        if depth == 0 or not game.get_legal_moves():
            return (-1, -1)

        else:
            best_score = float("-inf")
            best_move = (-1, -1)
            for move in game.get_legal_moves():
                score, _ = self.minimax_helper(game.forecast_move(move), depth - 1, False)
                if score > best_score:
                    best_move = move
                    best_score = score

            return best_move

    def minimax_helper(self, game, depth, max_player=True):
        """
        Gets called by minimax() method and performs the min and max functions recursively
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise SearchTimeout()

        if depth == 0 or not game.get_legal_moves():
            return self.score(game, self), (-1, -1)

        if max_player == True:
            best_score = float("-inf")
            best_move = (-1, -1)
            for move in game.get_legal_moves():
                score, _ = self.minimax_helper(game.forecast_move(move), depth - 1, False)
                if score > best_score:
                    best_score = score
                    best_move = move
            return best_score, best_move

        if max_player == False:
            best_score = float("inf")
            best_move = (-1, -1)
            for move in game.get_legal_moves():
                score, _ = self.minimax_helper(game.forecast_move(move), depth - 1, True)
                if score < best_score:
                    best_score = score
                    best_move = move
            return best_score, best_move


class AlphaBetaPlayer(IsolationPlayer):
    """Game-playing agent that chooses a move using iterative deepening minimax
    search with alpha-beta pruning. You must finish and test this player to
    make sure it returns a good move before the search time limit expires.
    """

    def get_move(self, game, time_left):
        """Search for the best move from the available legal moves and return a
        result before the time limit expires.

        Modify the get_move() method from the MinimaxPlayer class to implement
        iterative deepening search instead of fixed-depth search.

        **********************************************************************
        NOTE: If time_left() < 0 when this function returns, the agent will
              forfeit the game due to timeout. You must return _before_ the
              timer reaches 0.
        **********************************************************************

        Parameters
        ----------
        game : `isolation.Board`
            An instance of `isolation.Board` encoding the current state of the
            game (e.g., player locations and blocked cells).

        time_left : callable
            A function that returns the number of milliseconds left in the
            current turn. Returning with any less than 0 ms remaining forfeits
            the game.

        Returns
        -------
        (int, int)
            Board coordinates corresponding to a legal move; may return
            (-1, -1) if there are no available legal moves.
        """
        self.time_left = time_left

        best_move = (-1, -1)

        try:
            iterative_depth = 1
            while 1 == 1:
                best_move = self.alphabeta(game, iterative_depth)
                iterative_depth += 1

        except SearchTimeout:
            pass

        return best_move

    def my_turn(self, game):
        """
        returns true when it's max players turn
        """
        return game.active_player == self

    def alphabeta(self, game, depth, alpha=float("-inf"), beta=float("inf")):
        """Implement depth-limited minimax search with alpha-beta pruning as
        described in the lectures.

        This should be a modified version of ALPHA-BETA-SEARCH in the AIMA text
        https://github.com/aimacode/aima-pseudocode/blob/master/md/Alpha-Beta-Search.md

        **********************************************************************
            You MAY add additional methods to this class, or define helper
                 functions to implement the required functionality.
        **********************************************************************

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        alpha : float
            Alpha limits the lower bound of search on minimizing layers

        beta : float
            Beta limits the upper bound of search on maximizing layers

        Returns
        -------
        (int, int)
            The board coordinates of the best move found in the current search;
            (-1, -1) if there are no legal moves

        Notes
        -----
            (1) You MUST use the `self.score()` method for board evaluation
                to pass the project tests; you cannot call any other evaluation
                function directly.

            (2) If you use any helper functions (e.g., as shown in the AIMA
                pseudocode) then you must copy the timer check into the top of
                each helper function or else your agent will timeout during
                testing.
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise SearchTimeout()

        if not game.get_legal_moves():
            return (-1, -1)

        if depth == 0:
            return game.get_legal_moves()

        if self.my_turn(game):
            best_score = float("-inf")
            best_move = (-1, -1)
            alpha = float("-inf")
            beta = float("inf")
            for move in game.get_legal_moves():
                score, _ = self.alphabeta_helper(game.forecast_move(move), depth - 1, False, alpha, beta)
                if score > best_score:
                    best_score = score
                    best_move = move
                alpha = max(best_score, alpha)
            return best_move

        elif not self.my_turn(game):
            best_score = float("inf")
            best_move = (-1, -1)
            alpha = float("-inf")
            beta = float("inf")
            for move in game.get_legal_moves():
                score, _ = self.alphabeta_helper(game.forecast_move(move), depth - 1, True, alpha, beta)
                if score < best_score:
                    best_score = score
                    best_move = move
                beta = min(best_score, beta)
            return best_move

    def alphabeta_helper(self, game, depth, max_player=True, alpha=float("-inf"), beta=float("inf")):
        """
        Executes the aplphabeta algorythm
        :return: best move and the heurisitc for this move
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise SearchTimeout()

        legal_moves = game.get_legal_moves()

        if depth == 0 or not legal_moves:
            return self.score(game, self), (-1, -1)

        if max_player == True:
            best_move = (-1, -1)
            best_score = float("-inf")
            for m in legal_moves:
                score, _ = self.alphabeta_helper(game.forecast_move(m), depth - 1, False, alpha, beta)
                if score > best_score:
                    best_score = score
                    best_move = m
                if best_score >= beta:
                    return best_score, m
                alpha = max(best_score, alpha)
            return best_score, best_move

        elif max_player == False:
            best_move = (-1, -1)
            best_score = float("inf")
            for m in legal_moves:
                score, _ = self.alphabeta_helper(game.forecast_move(m), depth - 1, True, alpha, beta)
                if score < best_score:
                    best_score = score
                    best_move = m
                if best_score <= alpha:
                    return best_score, m
                beta = min(best_score, beta)
            return best_score, best_move
